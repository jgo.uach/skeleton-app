Buen día.

El enfoque de este proyecto es crear una Skeleton App para un fake login (como si fuera de Facebook, Google, etc) en el cual me enfoque mas en clean architecture que en temas de UI

Funcionamiento:
El app retrae los datos del layer "data" ya sea por retrofit o por DB. Si la DB no tiene la info o la cache esta expirada (compara fechas en formato dd/MM/yyyy), los datos se obtendran del API. En este caso se tiene un dummy object y una funcion null para probar casos de error.

Dependiendo de si el modelo es null o no, se mostrara la alerta correspondiente.

Librerias usadas:

*Navigation
*Dagger Hilt
*Room
*Retrofit 2 (solo de muestra, realmente solo se usa para los gson)
*LiveData
*Coroutines
*Mockk

Videos:

Prueba del app:
https://drive.google.com/file/d/1qHutLG021ju-YGP2QkMIdmKsmugjN7IM/view?usp=sharing

Explicacion de todo el proceso (muy largo el video, perdon >': ):
https://drive.google.com/file/d/1B1VXgaC1dLUOdywWU5j7ec0v2IRWyC9l/view?usp=sharing
