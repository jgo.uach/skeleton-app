package com.zshady.skeletonapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zshady.skeletonapp.data.db.dao.UserDao
import com.zshady.skeletonapp.data.db.entities.UserCache

@Database(entities = [UserCache::class], version = 1)
abstract class SkeletonDB: RoomDatabase() {
    abstract fun getUserDao(): UserDao
}