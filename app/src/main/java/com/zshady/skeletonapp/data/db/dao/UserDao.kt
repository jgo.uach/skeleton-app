package com.zshady.skeletonapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zshady.skeletonapp.data.db.entities.UserCache

@Dao
interface UserDao{
    @Query("SELECT * FROM user_table")
    suspend fun getLastUser(): UserCache

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(pokemonCache: UserCache)

    @Query("DELETE FROM user_table")
    suspend fun deleteUser()
}