package com.zshady.skeletonapp.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class UserCache(
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "json") val json: String,
    @ColumnInfo(name = "lastUpdateDate") val lastUpdateDate: String
)