package com.zshady.skeletonapp.data.modules

import android.content.Context
import androidx.room.Room
import com.zshady.skeletonapp.data.db.SkeletonDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {
    private const val SKELETON_DATABASE_NAME = "skeleton_database"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, SkeletonDB::class.java, SKELETON_DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideQuoteDao(db: SkeletonDB) = db.getUserDao()
}