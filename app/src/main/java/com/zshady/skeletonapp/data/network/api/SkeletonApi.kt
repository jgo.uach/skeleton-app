package com.zshady.skeletonapp.data.network.api

import com.zshady.skeletonapp.data.network.models.UserModel
import retrofit2.Response
import retrofit2.http.GET

interface SkeletonApi {
    @GET("user")
    suspend fun getUser(): Response<UserModel>
}