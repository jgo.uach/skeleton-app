package com.zshady.skeletonapp.data.network.models

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("user_id")
    val id: Int = 0,
    @SerializedName("user_name")
    val name: String = ""
)