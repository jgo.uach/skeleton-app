package com.zshady.skeletonapp.data.network.service

import com.zshady.skeletonapp.data.network.api.SkeletonApi
import com.zshady.skeletonapp.data.network.models.UserModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserService @Inject constructor(
    private val apiClient: SkeletonApi
) {
    suspend fun getUser(): UserModel?{
        return withContext(Dispatchers.IO){
            //We'll use a dummy user
            //apiClient.getUser().body()
            UserModel(1, "Usuario de prueba")
            //null
        }
    }
}