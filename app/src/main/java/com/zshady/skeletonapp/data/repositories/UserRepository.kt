package com.zshady.skeletonapp.data.repositories

import com.zshady.skeletonapp.data.db.dao.UserDao
import com.zshady.skeletonapp.data.db.entities.UserCache
import com.zshady.skeletonapp.data.network.models.UserModel
import com.zshady.skeletonapp.data.network.service.UserService
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userDao: UserDao,
    private val userService: UserService
) {
    suspend fun getUserFromApi(): UserModel?{
        return userService.getUser()
    }

    suspend fun getUserFromDb() : UserCache? {
        return userDao.getLastUser()
    }

    suspend fun insertUserToDb(userCache: UserCache){
        userDao.insertUser(userCache)
    }

    suspend fun clearUser(){
        userDao.deleteUser()
    }
}