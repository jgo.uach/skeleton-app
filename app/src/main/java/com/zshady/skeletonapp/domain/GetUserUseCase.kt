package com.zshady.skeletonapp.domain

import com.google.gson.Gson
import com.zshady.skeletonapp.data.db.entities.UserCache
import com.zshady.skeletonapp.data.network.models.UserModel
import com.zshady.skeletonapp.data.repositories.UserRepository
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class GetUserUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(): UserModel?{
        val userFromDb = userRepository.getUserFromDb()

        return if (userFromDb == null || Utils.isCacheExpired(userFromDb.lastUpdateDate)){
            val userFromNetwork = userRepository.getUserFromApi()

            if (userFromNetwork != null){
                userRepository.clearUser()
                userRepository.insertUserToDb(userFromNetwork.toEntity())
            }

            userFromNetwork
        } else
            Utils.formatJsonToUser(userFromDb.json)
    }

    private fun UserModel.toEntity(): UserCache {
        return UserCache(this.id, Utils.formatUserToJson(this), Utils.getCurrentTime())
    }
}

object Utils{

    private const val DATE_FORMAT = "dd/MM/yyyy"

    private var gsonInstance: Gson = Gson()
        get() {
            return if (field != null) field else {
                field = Gson()
                field
            }
        }

    fun getCurrentTime(): String{
        val sdf = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        return sdf.format(Date())
    }

    fun isCacheExpired(cacheDateString: String): Boolean {
        if (cacheDateString.isNullOrEmpty())
            return true

        val cacheDate: Date
        val sdf = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

        try {
            cacheDate = sdf.parse(cacheDateString)
        } catch (e: ParseException){
            return true
        }

        return cacheDate.compareTo(Date()) != 0
    }

    fun formatJsonToUser(json: String): UserModel{
        return gsonInstance.fromJson(json, UserModel::class.java)
    }

    fun formatUserToJson(userModel: UserModel): String{
        return gsonInstance.toJson(userModel)
    }
}