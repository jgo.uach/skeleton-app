package com.zshady.skeletonapp.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SkeletonApplication: Application()