package com.zshady.skeletonapp.ui.alerts

import android.app.AlertDialog
import android.content.Context

class AlertManager(val context: Context){
    fun showErrorAlert(){
        val builder = AlertDialog.Builder(context)

        builder.setTitle("Error")
        builder.setMessage("No te pudiste loggear :(")
        builder.setNeutralButton("Ok nimodo") { dialog, which ->
        }

        builder.show()
    }

    fun showSuccessAlert(name: String){
        val builder = AlertDialog.Builder(context)

        builder.setTitle("Exito")
        builder.setMessage("Bienvenido $name")
        builder.setNeutralButton("Gracias :D") { dialog, which ->
        }

        builder.show()
    }
}