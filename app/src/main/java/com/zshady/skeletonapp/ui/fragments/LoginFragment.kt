package com.zshady.skeletonapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.zshady.skeletonapp.databinding.FragmentLoginBinding
import com.zshady.skeletonapp.ui.alerts.AlertManager
import com.zshady.skeletonapp.ui.viewmodels.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment: Fragment() {

    lateinit var binding: FragmentLoginBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)

        setupObservers()
        binding.btnLogin.setOnClickListener {
            viewModel.getUser()
        }

        return binding.root
    }

    private fun setupObservers(){
        viewModel.userData.observe(viewLifecycleOwner) {
            AlertManager(requireContext()).showSuccessAlert(it.name)
        }

        viewModel.onErrorData.observe(viewLifecycleOwner) {
            AlertManager(requireContext()).showErrorAlert()
        }
    }
}