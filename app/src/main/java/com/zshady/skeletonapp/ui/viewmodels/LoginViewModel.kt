package com.zshady.skeletonapp.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zshady.skeletonapp.data.network.models.UserModel
import com.zshady.skeletonapp.domain.GetUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val userUseCase: GetUserUseCase
): ViewModel() {

    val userData: LiveData<UserModel>
        get() = _userData
    val onErrorData: LiveData<Boolean>
        get() = _onErrorData

    private val _userData = MutableLiveData<UserModel>()
    private val _onErrorData = MutableLiveData<Boolean>()

    fun getUser(){
        viewModelScope.launch {
            userUseCase()?.let {
                _userData.postValue(it)
            }?: _onErrorData.postValue(true)
        }
    }
}