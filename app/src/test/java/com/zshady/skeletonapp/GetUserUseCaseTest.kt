package com.zshady.skeletonapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.zshady.skeletonapp.data.db.entities.UserCache
import com.zshady.skeletonapp.data.network.models.UserModel
import com.zshady.skeletonapp.data.repositories.UserRepository
import com.zshady.skeletonapp.domain.GetUserUseCase
import com.zshady.skeletonapp.ui.viewmodels.LoginViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class GetUserUseCaseTest {

    @get:Rule
    var rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: LoginViewModel
    private lateinit var useCase: GetUserUseCase

    @RelaxedMockK
    private lateinit var mockedUseCase: GetUserUseCase

    @RelaxedMockK
    private lateinit var mockedRepository: UserRepository

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        viewModel = LoginViewModel(mockedUseCase)
        useCase = GetUserUseCase(mockedRepository)
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun onAfter() {
        Dispatchers.resetMain()
    }

    //ViewModel testing
    @Test
    fun `when getuser method is called from viewmodel, get an user model`() = runTest {
        val userModel = UserModel(1, "Usuario de prueba")

        coEvery {
            mockedUseCase()
        }returns userModel

        viewModel.getUser()

        assert(viewModel.userData.value == userModel)
    }

    //UseCase testing
    @Test
    fun `when the Db is empty, get an user model from API`() = runTest {
        val userModel = UserModel(1, "Usuario de prueba")

        coEvery {
            mockedRepository.getUserFromApi()
        }returns userModel

        val response = useCase()

        coVerify(exactly = 1) { mockedRepository.getUserFromDb() }
        coVerify(exactly = 1) {mockedRepository.getUserFromApi()}
        coVerify(exactly = 1) { mockedRepository.clearUser() }
        coVerify(exactly = 1) {mockedRepository.insertUserToDb(any())}
        assert(userModel == response)
    }
}